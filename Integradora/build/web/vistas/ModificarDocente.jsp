<%-- 
    Document   : DocenteEspecifico
    Created on : 08-dic-2019, 23:16:49
    Author     : Key Lanuss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<%@taglib  prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="<%=context%>/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="shortcut icon" href="<%=context%>/img/u.jpg" />
        <link rel="stylesheet" href="<%=context%>/vendors/ti-icons/css/themify-icons.css">
        <link rel="stylesheet" href="<%=context%>/vendors/base/vendor.bundle.base.css">
        <title class="">UTEZ</title>
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <a class="navbar-brand brand-logo mr-5" ><img src="<%=context%>/img/logo.png" class="mr-2" alt="logo" /></a>
                    <a class="navbar-brand brand-logo-mini" ><img src="<%=context%>/img/u.jpg" alt="logo"/></a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="ti-view-list"></span>
                    </button>
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item dropdown mr-1">
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="messageDropdown">
                                <div class="item-thumbnail">
                                    <img src="images/faces/face4.jpg" alt="image" class="profile-pic">
                                </div>
                                </a>
                                <a class="dropdown-item">
                                    <div class="item-thumbnail">
                                        <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                                    </div>
                                </a>
                                <a class="dropdown-item">
                                    <div class="item-thumbnail">
                                        <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                                    </div>
                                    <div class="item-content flex-grow">
                                        <h6 class="ellipsis font-weight-normal"> Johnson
                                        </h6>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                <img src="images/faces/face28.jpg" alt="profile"/>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                <a class="dropdown-item">
                                    <i class="ti-power-off text-primary"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="ti-view-list"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="index.html">
                                <i class="ti-shield menu-icon"></i>
                                <span class="menu-title">Docentes</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                                <i class="ti-palette menu-icon"></i>
                                <span class="menu-title">Divisiones académicas</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic">
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <h4 class="font-weight-bold mb-0">Inicio</h4>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 grid-margin stretch-card">
                                <div class="card position-relative">
                                    <div class="card-body">
                                        <p class="card-title">Modificar docentes</p>

                                        <input type="hidden" name="bean.id"  value="<s:property value="bean.id"/>">
                                        <div class="form-group">
                                            <label>Nombre:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.nombre"/>" >

                                        </div>
                                        <div class="form-group">
                                            <label>Primer apellido:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.primerApellido"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Segundo apellido:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.segundoApellido"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Correo personal:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.correoPersonal"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Télefono celular:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.telefonoCelular"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Télefono de casa:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.telefonoCsas"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>CURP:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.curp"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Año de nacimiento:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.persona.anhoNacimiento"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Correo institucional:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.correoInst"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Cédula profesional:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.cedula"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Nivel máximo de estudio:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.nivelMaxAcademico"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Título:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.titulo"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Semblanza:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.semblanza"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Estado:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.estado"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Materia:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.materia"/>" > 
                                        </div>
                                        <div class="form-group">
                                            <label>Tipo de profesor:</label>
                                            <input class="form-control form-control-lg" value="<s:property value="bean.tipo.tipo"/>" > 
                                        </div>
                                        <center>
                                        <div class="form-group">
                                            <form action="consultarDocentes" method="POST">
                                                <input type="submit" value="Regresar" class="btn btn-success"/>
                                            </form>
                                        </div>
                                            <div class="form-group">
                                            <form action="modificarDocente" method="POST">
                                                <input type="submit" value="Modificar" class="btn btn-primary"/>
                                            </form>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->

        <footer class="footer">

            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 UTEZ</a>. Derechos reservados.</span> 
            </div>

        </footer>

        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<script src="vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/template.js"></script>
<script src="js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>
</html>
