/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class PreguntaDao {

    private Connection c;
    private ResultSet rs;
    private PreparedStatement pst;
    private List listaPreguntas = new ArrayList();

    final String SQL_AGREGAR_PREGUNTA = "insert into pregunta (pregunta)values(?)";
    final String SQL_CONSULTAR_PREGUNTAS = "select * from pregunta";

    public boolean registrarPregunta(PreguntaBean bean) {
        boolean resultado = false;
        try {      
            c = Conexion.getConnection();
            pst = c.prepareStatement(SQL_AGREGAR_PREGUNTA);
            pst.setString(1, bean.getPregunta());
            resultado = pst.executeUpdate() == 1;
            pst.close();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(PreguntaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (resultado == true) {
         System.out.println("HOLA PRRO");   
        }
        
        return resultado;
    }

    public List<PreguntaBean> consultarPreguntas() {
        System.out.println("**********************ENTRAAAAAAA*********************************");
        PreguntaBean preguntas = null;
         List<PreguntaBean> listaPreguntas = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from preguntas");
            rs = pst.executeQuery();
            if (rs.next()) {
                preguntas = new PreguntaBean();  
                preguntas.setId(rs.getInt("id"));
                preguntas.setPregunta(rs.getString("pregunta"));
                listaPreguntas.add(preguntas);
            }
            rs.close();
            c.close();
            pst.close();
        } catch (SQLException ex) {
            Logger.getLogger(PreguntaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaPreguntas;
    }
}
