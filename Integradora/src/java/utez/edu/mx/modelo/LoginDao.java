/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class LoginDao {

    private Connection c;
    private ResultSet rs;
    private PreparedStatement pstm;

    public LoginBean login(String usuario, String contrasenha) {
        LoginBean login = null;
        try {
            c = Conexion.getConnection();
            pstm = c.prepareStatement("select p.*,r.* from persona as p inner join usuario as u on fk_persona = p.id"
                    + " inner join rol as r on r.id = u.fk_rol where usuario=? and contrasenha =?");
            pstm.setString(1, usuario);
            pstm.setString(2, contrasenha);
            rs = pstm.executeQuery();
            if (rs.next() == true) {
                login = new LoginBean();
                PersonaBean persona = new PersonaBean();
                RolBean rol = new RolBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                persona.setCorreoPersonal(rs.getString("correo_personal"));
                persona.setTelefonoCsas(String.valueOf(rs.getInt("tel_casa")));
                persona.setCurp(rs.getString("curp"));
                persona.setAnhoNacimiento(rs.getString("anho_nac"));
                rol.setRol(rs.getString("rol"));
                login.setPersona(persona);
                login.setRol(rol);
            }
            rs.close();
            pstm.close();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(LoginDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return login;
    }
}
