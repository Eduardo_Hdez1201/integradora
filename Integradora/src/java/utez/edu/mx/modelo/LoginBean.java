/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class LoginBean {
    private int id;
    private String usuario;
    private String contrsenha;
    private PersonaBean persona;
    private RolBean rol;
 
  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrsenha() {
        return contrsenha;
    }

    public void setContrsenha(String contrsenha) {
        this.contrsenha = contrsenha;
    }

    public PersonaBean getPersona() {
        return persona;
    }

    public void setPersona(PersonaBean persona) {
        this.persona = persona;
    }

    public RolBean getRol() {
        return rol;
    }

    public void setRol(RolBean rol) {
        this.rol = rol;
    }
    
}
