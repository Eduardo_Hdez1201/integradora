/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class DivisionBean {
    private int id;
    private String nombre;
    private String abreviatura;
    private PersonaBean director;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public PersonaBean getDirector() {
        return director;
    }

    public void setDirector(PersonaBean director) {
        this.director = director;
    }
    
}
