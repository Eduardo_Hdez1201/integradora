/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class DivisionDao {
    private Connection c;
    private ResultSet rs;
    private PreparedStatement pst;
    
    public List<DivisionBean> getDivisiones() {
        List<DivisionBean> listaDivisiones = new ArrayList<>();
        DivisionBean division = null;
        PersonaBean persona = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select division.*,persona.nombre,persona.primer_apellido, segundo_apellido from division inner join persona on persona.id = division.director;");
            rs = pst.executeQuery();
            while (rs.next()) {
                persona = new PersonaBean();
                division = new DivisionBean();
                division.setId(rs.getInt("id"));
                division.setNombre(rs.getString("nombre_division"));
                division.setAbreviatura(rs.getString("abreviatura"));
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                division.setDirector(persona);
                listaDivisiones.add(division);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getDivisiones" + ex);
        }
        return listaDivisiones;
    }
    
    public List<CarreraBean> getCarreras() {
        List<CarreraBean> listaCarreras = new ArrayList<>();
        CarreraBean carrera = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from carrera;");
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                carrera.setId(rs.getInt("idCarrera"));
                carrera.setNombre(rs.getString("nombre_carrera"));
                listaCarreras.add(carrera);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getCarreras" + ex);
        }
        return listaCarreras;
    }
    
    private Connection con;
    private PreparedStatement ps;

    private String sqlRegistrarDivision = "INSERT INTO division VALUES(NULL,?,?,?);";

    public boolean registrarDivision(DivisionBean division) {
        boolean resultado = false;
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlRegistrarDivision);
            ps.setString(1, division.getNombre());
            ps.setString(2, division.getAbreviatura());
            ps.setInt(3, division.getDirector().getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: registrarDivision()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:registrarDivision()\n" + e);
            }
        }
        return resultado;
    }

    private String sqlModificarDivision = "UPDATE division SET nombre=?, abreviatura=?, director=? WHERE id = ?;";

    public boolean modificarDivision(DivisionBean division) {
        boolean resultado = false;
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlModificarDivision);
            ps.setString(1, division.getNombre());
            ps.setString(2, division.getAbreviatura());
            ps.setInt(3, division.getDirector().getId());
            ps.setInt(4, division.getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: modificarDivision()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:modificarDivision()\n" + e);
            }
        }
        return resultado;
    }

    private String sqlConsultarDivisionEspecifica = "SELECT d.id, d.nombre, d.abreviatura, p.id as idP, p.nombre as director, p.primer_apellido, p.segundo_apellido FROM division as d INNER JOIN persona as p ON d.director = p.id WHERE d.id = ?;";
    
    public DivisionBean consultarDatosDivision(DivisionBean divisionConsulta) {
        DivisionBean division = null;
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlConsultarDivisionEspecifica);
            ps.setInt(1, divisionConsulta.getId());
            rs = ps.executeQuery();
            if(rs.next()){
                division = new DivisionBean();
                division.setId(rs.getInt("id"));
                division.setNombre(rs.getString("nombre"));
                division.setAbreviatura(rs.getString("abreviatura"));
                
                PersonaBean director =new PersonaBean();
                director.setId(rs.getInt("idP"));
                director.setNombre(rs.getString("director"));
                director.setPrimerApellido(rs.getString("primer_apellido"));
                director.setSegundoApellido(rs.getString("segundo_apellido"));
                
                division.setDirector(director);
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: consultarDatosDivision()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:consultarDatosDivision()\n" + e);
            }
        }
        return division;
    }
    
    private String sqlEliminarDivision = "DELETE FROM division WHERE  id = ?;";
    private String sqlEliminarDivisionLogica = "UPDATE division SET director=null WHERE id = ?;";
    
    public boolean eliminarDivision(DivisionBean division) {
        boolean resultado = false;
        try {
            System.out.println("Conexión");
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlEliminarDivisionLogica);
            ps.setInt(1, division.getId());
            System.out.println(division.getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: eliminarDivision()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:eliminarDivision()\n" + e);
            }
        }
        return resultado;
    }
    
    private String sqlConsultarDivisiones = "SELECT d.id, d.nombre, d.abreviatura, p.id as idDirector, p.nombre as name, p.primer_apellido, p.segundo_apellido FROM division as d INNER JOIN persona as p ON d.director = p.id;";
    
    public List<DivisionBean> consultarDivisiones() {
        List<DivisionBean> divisiones = new ArrayList<DivisionBean>();
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlConsultarDivisiones);
            rs = ps.executeQuery();
            while(rs.next()){
                DivisionBean division = new DivisionBean();
                division.setId(rs.getInt("id"));
                division.setNombre(rs.getString("nombre"));
                division.setAbreviatura(rs.getString("abreviatura"));
                
                PersonaBean director =new PersonaBean();
                director.setId(rs.getInt("idDirector"));
                director.setNombre(rs.getString("name"));
                director.setPrimerApellido(rs.getString("primer_apellido"));
                director.setSegundoApellido(rs.getString("segundo_apellido"));
                
                division.setDirector(director);
                
                divisiones.add(division);
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: consultarDivisiones()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:consultarDivisiones()\n" + e);
            }
        }
        return divisiones;
    }
    
    private String sqlConsultarDirectores = "select p.id, p.nombre , p.primer_apellido, p.segundo_apellido from persona as p inner join usuario as u on p.id = u.id inner join rol as r on u.id = r.id where u.fk_rol = 3;";
    
    public List<PersonaBean> consultarDirectores() {
        List<PersonaBean> directores = new ArrayList<PersonaBean>();
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlConsultarDirectores);
            rs = ps.executeQuery();
            while(rs.next()){
                PersonaBean director =new PersonaBean();
                director.setId(rs.getInt("id"));
                director.setNombre(rs.getString("nombre"));
                director.setPrimerApellido(rs.getString("primer_apellido"));
                director.setSegundoApellido(rs.getString("segundo_apellido"));
                
                directores.add(director);
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:DivisionDao\nMétodo: consultarDirectores()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:DivisionDao\nMétodo:consultarDirectores()\n" + e);
            }
        }
        return directores;
    }
    
}
