/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class AspiranteDao {

    private Connection c;
    private PreparedStatement ps;
    private ResultSet rs;

    private final String SQL_AGREGAR_ASPIRANTE = "insert into persona(nombre,primer_apellido,segundo_apellido,correo_personal,tel_cel,tel_casa,"
            + "curp,anho_nac,sexo)"
            + "values(?,?,?,?,?,?,?,?,?)";
    private final String SQL_A = "insert into aspirante(calle,colonia,promedio,no_faltas,estado,prepa,estado_civil,persona,carrera)"
            + " values(?,?,?,0,?,?,?,?,?)";
    private final String SQL_USUARIO = "insert into usuario(usuario,contrasenha,fk_rol,fk_persona)values(?,?,4,?)";
    private final String SQL_ID = "select id from persona order by id desc limit 1";
    private final String SQL_LISTA_ESTADO_REPUBLICA = "select * from estado_rep";
    private final String SQL_LISTA_PREPARATORIAS = "select * from prepa";
    private final String SQL_LISTA_ESTADO_CIVIL = "select * from estado_civil";
    private final String SQL_LISTA_CARRERA = "select * from carrera";

    public boolean registroAspirante(AspiranteBean bean, String usuario, String pass) {
        boolean resultado = false;
        int id;
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_AGREGAR_ASPIRANTE);
            ps.setString(1, bean.getPersona().getNombre());
            ps.setString(2, bean.getPersona().getPrimerApellido());
            ps.setString(3, bean.getPersona().getSegundoApellido());
            ps.setString(4, bean.getPersona().getCorreoPersonal());
            ps.setString(5, bean.getPersona().getTelefonoCelular());
            ps.setString(6, bean.getPersona().getTelefonoCsas());
            ps.setString(7, bean.getPersona().getCurp());
            ps.setString(8, bean.getPersona().getAnhoNacimiento());
            ps.setString(9, bean.getPersona().getSexo());

            resultado = ps.executeUpdate() == 1;
            ps.close();
            c.close();
            if (resultado) {
                c = Conexion.getConnection();
                ps = c.prepareStatement(SQL_ID);
                rs = ps.executeQuery();
                id = rs.getInt("id");
                ps.close();
                c.close();
                rs.close();

                c = Conexion.getConnection();
                ps = c.prepareStatement(SQL_A);

                ps.setString(1, bean.getCalle());
                ps.setString(2, bean.getColonia());
                ps.setDouble(2, bean.getPromedio());
                ps.setInt(3, bean.getEstado().getId());
                ps.setInt(4, bean.getPrepa().getId());
                ps.setInt(5, bean.getEstadoCivil().getId());
                ps.setInt(6, id);
                ps.setInt(7, bean.getCarrera().getId());

                resultado = ps.executeUpdate() == 1;
                ps.close();
                c.close();

                c = Conexion.getConnection();
                ps = c.prepareStatement(SQL_USUARIO);
                ps.setString(1, usuario);
                ps.setString(2, pass);
                ps.setInt(3, id);

                resultado = ps.executeUpdate() == 1;
                ps.close();
                c.close();
            }

        } catch (SQLException e) {
            System.out.println("Error en el método registro aspirante " + e.getMessage());
            try {
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        } finally {
            try {
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        }
        return resultado;
    }

    public ArrayList<EstadoRepBean> consultaEstadosRepublica() {
        EstadoRepBean edo = null;
        ArrayList<EstadoRepBean> lista = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_LISTA_ESTADO_REPUBLICA);
            rs = ps.executeQuery();
            while (rs.next()) {
                edo = new EstadoRepBean();
                edo.setId(rs.getInt("id"));
                edo.setEstadoRep(rs.getString("nombre"));

                lista.add(edo);
            }
            rs.close();
            ps.close();
            c.close();
        } catch (SQLException e) {
            System.out.println("Error en el método registro aspirante " + e.getMessage());
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        }
        return lista;
    }

    public ArrayList<PreparatoriaBean> consultaPreparatorias() {
        PreparatoriaBean edo = null;
        ArrayList<PreparatoriaBean> lista = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_LISTA_PREPARATORIAS);
            rs = ps.executeQuery();
            while (rs.next()) {
                edo = new PreparatoriaBean();
                edo.setId(rs.getInt("id"));
                edo.setNombrePrepa(rs.getString("nombre"));

                lista.add(edo);
            }
            rs.close();
            ps.close();
            c.close();
        } catch (SQLException e) {
            System.out.println("Error en el método registro aspirante " + e.getMessage());
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        }
        return lista;
    }

    public ArrayList<EstadoCivilBean> consultaEstadoCivil() {
        EstadoCivilBean edo = null;
        ArrayList<EstadoCivilBean> lista = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_LISTA_ESTADO_CIVIL);
            rs = ps.executeQuery();
            while (rs.next()) {
                edo = new EstadoCivilBean();
                edo.setId(rs.getInt("id"));
                edo.setEstadoCivil(rs.getString("edo_civil"));

                lista.add(edo);
            }
            rs.close();
            ps.close();
            c.close();
        } catch (SQLException e) {
            System.out.println("Error en el método registro aspirante " + e.getMessage());
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        }
        return lista;
    }

    public ArrayList<CarreraBean> consultaCarreras() {
        CarreraBean edo = null;
        ArrayList<CarreraBean> lista = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_LISTA_CARRERA);
            rs = ps.executeQuery();
            while (rs.next()) {
                edo = new CarreraBean();
                edo.setId(rs.getInt("id"));
                edo.setNombre(rs.getString("nombre"));

                lista.add(edo);
            }
            rs.close();
            ps.close();
            c.close();
        } catch (SQLException e) {
            System.out.println("Error en el método registro aspirante " + e.getMessage());
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (SQLException ex) {
                System.out.println("Error al cerrar conexiones " + ex.getMessage());
            }
        }
        return lista;
    }
}
