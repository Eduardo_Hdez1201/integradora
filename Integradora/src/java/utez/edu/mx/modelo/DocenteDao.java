/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class DocenteDao {

    private Connection c = null;
    private CallableStatement cs = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    final private String SQL_CONSULTAR_DOCENTE = "select p.nombre, p.primer_apellido, p.segundo_apellido, d.id AS idDocente, d.correo_inst, d.titulo, t.tipo\n"
            + "from persona as p inner join docente as d on d.persona = p.id inner join tipo_doc as t on \n"
            + "t.id = d.tipo;";
    final private String SQL_CONSULTAR_DOCENTE_ESPECIFICO = "select p.id AS idPersona, p.nombre,p.primer_apellido,p.segundo_apellido,p.correo_personal,p.tel_cel,p.tel_casa,p.curp,p.anho_nac,\n"
            + "d.id AS idDocente, d.correo_inst,d.cedula,d.nivel_max_academico,d.titulo,d.semblanza,d.estado,d.materia,t.id AS idTipo,t.tipo\n"
            + "from persona as p inner join docente as d on d.persona = p.id inner join tipo_doc as t on \n"
            + "t.id = d.tipo WHERE d.id = ?;";
    final private String SQL_AGREGAR_DOCENTE = "{call insertar_alumno(?,?,?,?,?,?,?,?,?,?)}";
    final private String SQL_ELIMINAR_DOCENTE = "delete from docente where id = ?";
    final private String SQL_MODIFICAR_DOCENTE = "{call modificar_docente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)} ";

    public List<DocenteBean> consultarDocentes() {
        List<DocenteBean> lista = new ArrayList<>();
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_CONSULTAR_DOCENTE);
            rs = ps.executeQuery();
            while (rs.next()) {

                PersonaBean p = new PersonaBean();
                DocenteBean e = new DocenteBean();
                TipoDocBean t = new TipoDocBean();

                p.setNombre(rs.getString("p.nombre"));
                p.setPrimerApellido(rs.getString("p.primer_apellido"));
                p.setSegundoApellido(rs.getString("p.segundo_apellido"));
                e.setPersona(p);

                e.setId(rs.getInt("idDocente"));
                e.setCorreoInst(rs.getString("d.correo_inst"));
                e.setTitulo(rs.getString("d.titulo"));

               // t.setId(rs.getInt("idTipo"));
                t.setTipo(rs.getString("t.tipo"));
                e.setTipo(t);

                lista.add(e);
            }

            rs.close();
            ps.close();
            c.close();
        } catch (Exception e) {
            System.out.println("Error en el método consultarDocentes() " + e.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (Exception e) {
                System.out.println("Error al cerrar conexiones consultarDocentes() " + e.getMessage());
            }
        }
        return lista;
    }

    public DocenteBean consultarDocentesId(int id) {
        DocenteBean ee = null;
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_CONSULTAR_DOCENTE_ESPECIFICO);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ee = new DocenteBean();
                PersonaBean p = new PersonaBean();
                TipoDocBean t = new TipoDocBean();

                p.setId(rs.getInt("idPersona"));
                p.setNombre(rs.getString("p.nombre"));
                p.setPrimerApellido(rs.getString("p.primer_apellido"));
                p.setSegundoApellido(rs.getString("p.segundo_apellido"));
                p.setCorreoPersonal(rs.getString("p.correo_personal"));
                p.setTelefonoCelular(rs.getString("p.tel_cel"));
                p.setTelefonoCsas(rs.getString("p.tel_casa"));
                p.setCurp(rs.getString("p.curp"));
                p.setAnhoNacimiento(rs.getString("p.anho_nac"));
                ee.setPersona(p);

                ee.setId(rs.getInt("idDocente"));
                ee.setCorreoInst(rs.getString("d.correo_inst"));
                ee.setCedula(rs.getString("d.cedula"));
                ee.setNivelMaxAcademico(rs.getString("d.nivel_max_academico"));
                ee.setTitulo(rs.getString("d.titulo"));
                ee.setSemblanza(rs.getString("d.semblanza"));
                ee.setEstado(rs.getBoolean("d.estado"));
                ee.setMateria(rs.getString("d.materia"));

                t.setId(rs.getInt("idTipo"));
                t.setTipo(rs.getString("t.tipo"));
                ee.setTipo(t);
            }

            rs.close();
            ps.close();
            c.close();
        } catch (Exception e) {
            System.out.println("Error en el método consultarDocentesId() " + e.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (Exception e) {
                System.out.println("Error al cerrar conexiones consultarDocentesId() " + e.getMessage());
            }
        }
        return ee;
    }
    
     public DocenteBean consultarDocentesIdM(int id) {
        DocenteBean ee = null;
        try {
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_CONSULTAR_DOCENTE_ESPECIFICO);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                ee = new DocenteBean();
                PersonaBean p = new PersonaBean();
                TipoDocBean t = new TipoDocBean();

                p.setId(rs.getInt("idPersona"));
                p.setNombre(rs.getString("p.nombre"));
                p.setPrimerApellido(rs.getString("p.primer_apellido"));
                p.setSegundoApellido(rs.getString("p.segundo_apellido"));
                p.setCorreoPersonal(rs.getString("p.correo_personal"));
                p.setTelefonoCelular(rs.getString("p.tel_cel"));
                p.setTelefonoCsas(rs.getString("p.tel_casa"));
                p.setCurp(rs.getString("p.curp"));
                p.setAnhoNacimiento(rs.getString("p.anho_nac"));
                ee.setPersona(p);

                ee.setId(rs.getInt("idDocente"));
                ee.setCorreoInst(rs.getString("d.correo_inst"));
                ee.setCedula(rs.getString("d.cedula"));
                ee.setNivelMaxAcademico(rs.getString("d.nivel_max_academico"));
                ee.setTitulo(rs.getString("d.titulo"));
                ee.setSemblanza(rs.getString("d.semblanza"));
                ee.setEstado(rs.getBoolean("d.estado"));
                ee.setMateria(rs.getString("d.materia"));

                t.setId(rs.getInt("idTipo"));
                t.setTipo(rs.getString("t.tipo"));
                ee.setTipo(t);
            }

            rs.close();
            ps.close();
            c.close();
        } catch (Exception e) {
            System.out.println("Error en el método consultarDocentesId() " + e.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
                c.close();
            } catch (Exception e) {
                System.out.println("Error al cerrar conexiones consultarDocentesId() " + e.getMessage());
            }
        }
        return ee;
    }
     
     
      public boolean EliminarDocente(int idDocente) {
        boolean status = false;
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement pstm = con.prepareStatement(SQL_ELIMINAR_DOCENTE);
            pstm.setInt(1, idDocente);
            if (pstm.executeUpdate() == 1) {
                status = true;
            }
            pstm.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;

    }
     
    public boolean modificarDocente(DocenteBean bean) {
        boolean resu = false;
        try {
            PersonaBean p = new PersonaBean();
            c = Conexion.getConnection();
            ps = c.prepareStatement(SQL_MODIFICAR_DOCENTE);
            p.setId(rs.getInt("idPersona"));
                p.setNombre(rs.getString("nombre"));
                p.setPrimerApellido(rs.getString("primer_apellido"));
                p.setSegundoApellido(rs.getString("segundo_apellido"));
                p.setCorreoPersonal(rs.getString("correo_personal"));
                p.setTelefonoCelular(rs.getString("tel_cel"));
                p.setTelefonoCsas(rs.getString("tel_casa"));
                p.setCurp(rs.getString("curp"));
                p.setAnhoNacimiento(rs.getString("anho_nac"));
                bean.setPersona(p);

                bean.setId(rs.getInt("id"));
                bean.setCorreoInst(rs.getString("correo_inst"));
                bean.setCedula(rs.getString("cedula"));
                bean.setNivelMaxAcademico(rs.getString("nivel_max_academico"));
                bean.setTitulo(rs.getString("titulo"));
                bean.setSemblanza(rs.getString("semblanza"));
                bean.setEstado(rs.getBoolean("estado"));
                bean.setMateria(rs.getString("materia"));

               

            resu = ps.executeUpdate() == 1;
            ps.close();
            c.close();
        } catch (Exception e) {
            System.out.println("Error en el método modificarDulce() del DaoDulce -> " + e.getMessage());
        } finally {
            try {
                ps.close();
                c.close();
            } catch (Exception e) {
                System.out.println("No se cerrarón las conexiones en modificarDulce() del DaoDulce -> " + e.getMessage());
            }
        }
        return resu;
    }  
      

//    public static void main(String[] args) {
//        DocenteDao dao = new DocenteDao();
//        DocenteBean bean = new DocenteBean();
//
//        bean = dao.consultarDocentesId(1);
//
//        System.out.println(bean.getPersona().getNombre());
//    }
}

