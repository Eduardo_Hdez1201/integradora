/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class PersonaBean {
 private int id;
 private String nombre;
 private String primerApellido;
 private String segundoApellido;
 private String correoPersonal;
 private String telefonoCelular;
 private String telefonoCsas;
 private String curp;
 private String anhoNacimiento;
 private String sexo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCorreoPersonal() {
        return correoPersonal;
    }

    public void setCorreoPersonal(String correoPersonal) {
        this.correoPersonal = correoPersonal;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    public String getTelefonoCsas() {
        return telefonoCsas;
    }

    public void setTelefonoCsas(String telefonoCsas) {
        this.telefonoCsas = telefonoCsas;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getAnhoNacimiento() {
        return anhoNacimiento;
    }

    public void setAnhoNacimiento(String anhoNacimiento) {
        this.anhoNacimiento = anhoNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
 
 
 
}
