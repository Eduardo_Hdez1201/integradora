/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class GrupoDao {

    private Connection c;
    private ResultSet rs;
    private PreparedStatement pst;

    public boolean registrarGrupo(CarreraBean beanC, GrupoBean bean, DocenteBean beanD) {
        boolean result = false;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("insert into grupo (nombre_grupo, carrera) values(?,?)");
            pst.setString(1, bean.getNombre());
            pst.setInt(2, beanC.getId());
            result = pst.executeUpdate() == 1;

        } catch (SQLException ex) {
            System.err.println("Error registrarGrupo" + ex);
        }

        return result;
    }

    public boolean registrarGrupoDocente(GrupoBean bean, DocenteBean beanD) {
        boolean result = false;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from grupo_docente where grupo = ? and (select count(docente) from grupo_docente where grupo = ?) >= 2;");
            pst.setInt(1, bean.getId());
            pst.setInt(2, bean.getId());
            rs = pst.executeQuery();
            if (rs.next() == false) {
                pst = c.prepareStatement("insert into grupo_docente (docente, grupo) values(?,?)");
                pst.setInt(1, beanD.getId());
                pst.setInt(2, bean.getId());
                result = pst.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            System.err.println("Error registrarGrupoDocente" + ex);
        }

        return result;
    }

    public boolean registrarGrupoAspirante(GrupoBean bean, AspiranteBean beanA) {
        boolean result = false;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("update aspirante set grupo=? where id = ?");
            pst.setInt(1, bean.getId());
            pst.setInt(2, beanA.getId());
            result = pst.executeUpdate() == 1;
        } catch (SQLException ex) {
            System.err.println("Error registrarGrupoDocente" + ex);
        }

        return result;
    }

    public GrupoBean consultaEspecificaGrupo(GrupoBean bean) {
        GrupoBean grupo = null;
        PersonaBean persona = null;
        DocenteBean docente = null;
        CarreraBean carrera = null;
        boolean result = false;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select grupo.*, carrera.nombre_carrera from grupo inner join carrera on carrera.idCarrera = grupo.carrera where id=?;");
            pst.setInt(1, bean.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                grupo = new GrupoBean();
                carrera.setNombre(rs.getString("nombre_carrera"));
                grupo.setNombre(rs.getString("nombre_grupo"));
                grupo.setCarrera(carrera);
                grupo.setId(rs.getInt("id"));
            }
        } catch (SQLException ex) {
            System.err.println("Error getGrupoEspecifico" + ex);
        }
        return grupo;
    }

    public boolean eliminarGrupo(GrupoBean bean) {
        boolean result = false;
        System.out.println(bean.getId());
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("delete from grupo_docente where grupo = ?");
            pst.setInt(1, bean.getId());
            result = pst.executeUpdate() == 1;
            if (result) {
                pst = c.prepareStatement("delete from grupo where id= ?");
                pst.setInt(1, bean.getId());
                result = pst.executeUpdate() == 1;
            } else {
                pst = c.prepareStatement("delete from grupo where id= ?");
                pst.setInt(1, bean.getId());
                result = pst.executeUpdate() == 1;
            }

        } catch (SQLException ex) {
            System.err.println("Error eliminarGrupo" + ex);
        }
        return result;
    }

    public boolean eliminarGrupoDocente(GrupoBean bean) {
        boolean result = false;
        System.out.println(bean.getId());
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select grupo from aspirante where grupo = ?");
            pst.setInt(1, bean.getId());
            rs = pst.executeQuery();
            if (rs.next()) {
                pst = c.prepareStatement("update aspirante set grupo = null where grupo = ?;");
                pst.setInt(1, bean.getId());
                result = pst.executeUpdate() == 1;
                if (result) {
                    pst = c.prepareStatement("delete from grupo_docente where id = ?");
                    pst.setInt(1, bean.getId());
                    result = pst.executeUpdate() == 1;
                }
            } else {
                pst = c.prepareStatement("delete from grupo_docente where id = ?");
                pst.setInt(1, bean.getId());
                result = pst.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            System.err.println("Error eliminarGrupoDocente" + ex);
        }
        return result;
    }

    public List<GrupoBean> getGruposDocente() {
        List<GrupoBean> listaGrupos = new ArrayList<>();
        GrupoBean grupo = null;
        PersonaBean persona = null;
        DocenteBean docente = null;
        CarreraBean carrera = null;
        DivisionBean division = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select grupo_docente.docente,grupo_docente.id,persona.nombre,persona.primer_apellido,persona.segundo_apellido,docente.materia, division.nombre_division,carrera.nombre_carrera, grupo.nombre_grupo from grupo_docente "
                    + "inner join docente on grupo_docente.docente = docente.id inner join persona on persona.id = docente.persona inner join grupo on grupo.id = grupo_docente.grupo "
                    + "inner join carrera on carrera.idCarrera = grupo.carrera inner join division on division.id = division.id;");
            rs = pst.executeQuery();
            while (rs.next()) {
                persona = new PersonaBean();
                carrera = new CarreraBean();
                grupo = new GrupoBean();
                docente = new DocenteBean();
                division = new DivisionBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                division.setNombre(rs.getString("nombre_division"));
                carrera.setNombre(rs.getString("nombre_carrera"));
                grupo.setNombre(rs.getString("nombre_grupo"));
                docente.setPersona(persona);
                carrera.setDivision(division);
                grupo.setCarrera(carrera);
                grupo.setDocente(docente);
                grupo.setId(rs.getInt("id"));

                listaGrupos.add(grupo);
            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getCarreras" + ex);
        }

        return listaGrupos;
    }

    public List<GrupoBean> getGrupos() {
        List<GrupoBean> listaGrupos = new ArrayList<>();
        GrupoBean grupo = null;
        PersonaBean persona = null;
        DocenteBean docente = null;
        CarreraBean carrera = null;
        DivisionBean division = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from grupo where carrera = ?");
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                grupo = new GrupoBean();
                division = new DivisionBean();
                division.setNombre(rs.getString("nombre_division"));
                carrera.setNombre(rs.getString("nombre_carrera"));
                grupo.setNombre(rs.getString("nombre_grupo"));
                carrera.setDivision(division);
                grupo.setCarrera(carrera);
                grupo.setDocente(docente);
                grupo.setId(rs.getInt("id"));

                listaGrupos.add(grupo);
            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getCarreras" + ex);
        }

        return listaGrupos;
    }

    public List<GrupoBean> getGruposCarrera(CarreraBean beanC) {
        List<GrupoBean> listaGrupos = new ArrayList<>();
        GrupoBean grupo = null;
        PersonaBean persona = null;
        DocenteBean docente = null;
        CarreraBean carrera = null;
        DivisionBean division = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from grupo where carrera = ?;");
            pst.setInt(1, beanC.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                grupo = new GrupoBean();
                grupo.setId(rs.getInt("id"));
                grupo.setNombre(rs.getString("nombre_grupo"));

                listaGrupos.add(grupo);
            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getGruposCarreras" + ex);
        }

        return listaGrupos;
    }

    public List<DivisionBean> getDivisiones() {
        List<DivisionBean> listaDivisiones = new ArrayList<>();
        DivisionBean division = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from division;");
            rs = pst.executeQuery();
            while (rs.next()) {
                division = new DivisionBean();
                division.setNombre(rs.getString("nombre_division"));
                division.setAbreviatura(rs.getString("abreviatura"));

                listaDivisiones.add(division);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getDivisiones" + ex);
        }
        return listaDivisiones;
    }

    public List<CarreraBean> getCarreras() {
        List<CarreraBean> listaCarreras = new ArrayList<>();
        CarreraBean carrera = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from carrera;");
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                carrera.setId(rs.getInt("idCarrera"));
                carrera.setNombre(rs.getString("nombre_carrera"));

                listaCarreras.add(carrera);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getCarreras" + ex);
        }
        return listaCarreras;
    }

    public CarreraBean getCarreraEspecifica(CarreraBean beanC) {
        CarreraBean carrera = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from carrera where idCarrera= ?;");
            pst.setInt(1, beanC.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                carrera.setId(rs.getInt("idCarrera"));
                carrera.setNombre(rs.getString("nombre_carrera"));

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getCarreraEspecificas" + ex);
        }
        return carrera;
    }
    
    
        public List<AspiranteBean> getAspirantes() {
        List<AspiranteBean> listaAspirantes = new ArrayList<>();
        AspiranteBean aspirante = null;
        PersonaBean persona = null;

        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select persona.nombre,persona.primer_apellido,persona.segundo_apellido,aspirante.grupo,aspirante.id from aspirante inner join persona on persona.id = aspirante.persona where grupo IS NULL ORDER BY primer_apellido DESC;");
            rs = pst.executeQuery();
            while (rs.next()) {
                aspirante = new AspiranteBean();
                persona = new PersonaBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                aspirante.setId(rs.getInt("id"));
                aspirante.setPersona(persona);

                listaAspirantes.add(aspirante);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getAspirantes" + ex);
        }
        return listaAspirantes;
    }
    
    public List<AspiranteBean> getAspirantesGrupo(GrupoBean bean, AspiranteBean beanA) {
        List<AspiranteBean> listaAspirantes = new ArrayList<>();
        AspiranteBean aspirante = null;
        PersonaBean persona = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select persona.nombre,persona.primer_apellido, persona.segundo_apellido, aspirante.grupo from aspirante inner join persona on persona.id = aspirante.persona where grupo = ?");
            System.out.println(bean.getId());
            pst.setInt(1, bean.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                aspirante = new AspiranteBean();
                persona = new PersonaBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                aspirante.setPersona(persona);

                listaAspirantes.add(aspirante);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getAspirantesGrupo" + ex);
        }
        return listaAspirantes;
    }

    public List<DocenteBean> getDocentes() {
        List<DocenteBean> listaDocentes = new ArrayList<>();
        DocenteBean docente = null;
        PersonaBean persona = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select persona.nombre,persona.primer_apellido,persona.segundo_apellido,docente.materia,docente.id from docente inner join persona on persona.id = docente.persona;");
            rs = pst.executeQuery();
            while (rs.next()) {
                docente = new DocenteBean();
                persona = new PersonaBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                docente.setId(rs.getInt("id"));
                docente.setMateria(rs.getString("materia"));
                docente.setPersona(persona);

                listaDocentes.add(docente);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getDocentes" + ex);
        }
        return listaDocentes;
    }

    public List<DocenteBean> getDocentesGrupo(GrupoBean bean) {
        List<DocenteBean> listaDocentes = new ArrayList<>();
        DocenteBean docente = null;
        PersonaBean persona = null;
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select docente.id,docente.materia,grupo_docente.docente,persona.nombre,persona.primer_apellido,persona.segundo_apellido from grupo_docente inner join docente on docente.id = grupo_docente.docente inner join persona on persona.id = docente.persona where grupo = ?");
            pst.setInt(1, bean.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                docente = new DocenteBean();
                persona = new PersonaBean();
                persona.setNombre(rs.getString("nombre"));
                persona.setPrimerApellido(rs.getString("primer_apellido"));
                persona.setSegundoApellido(rs.getString("segundo_apellido"));
                docente.setId(rs.getInt("id"));
                docente.setMateria(rs.getString("materia"));
                docente.setPersona(persona);

                listaDocentes.add(docente);
                System.out.println(listaDocentes);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getDocentes" + ex);
        }
        return listaDocentes;
    }
}
