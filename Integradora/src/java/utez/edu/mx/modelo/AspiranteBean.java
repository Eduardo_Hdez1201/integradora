/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class AspiranteBean {
    private int id;
    private String calle;
    private String colonia;
    private double promedio;
    private EstadoRepBean estado;
    private EstadoCivilBean estadoCivil;
    private PreparatoriaBean prepa;
    private PersonaBean persona;
    private CarreraBean carrera;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public EstadoRepBean getEstado() {
        return estado;
    }

    public void setEstado(EstadoRepBean estado) {
        this.estado = estado;
    }

    public EstadoCivilBean getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivilBean estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public PreparatoriaBean getPrepa() {
        return prepa;
    }

    public void setPrepa(PreparatoriaBean prepa) {
        this.prepa = prepa;
    }

    public PersonaBean getPersona() {
        return persona;
    }

    public void setPersona(PersonaBean persona) {
        this.persona = persona;
    }

    public CarreraBean getCarrera() {
        return carrera;
    }

    public void setCarrera(CarreraBean carrera) {
        this.carrera = carrera;
    }
    
    
}
