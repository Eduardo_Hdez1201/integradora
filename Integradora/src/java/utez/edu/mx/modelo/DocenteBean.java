/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class DocenteBean {

    private int id;
    private String correoInst;
    private String cedula;
    private String nivelMaxAcademico;
    private String titulo;
    private String semblanza;
    private boolean estado;
    private String materia;
    private TipoDocBean tipo;
    private PersonaBean persona;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCorreoInst() {
        return correoInst;
    }

    public void setCorreoInst(String correoInst) {
        this.correoInst = correoInst;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNivelMaxAcademico() {
        return nivelMaxAcademico;
    }

    public void setNivelMaxAcademico(String nivelMaxAcademico) {
        this.nivelMaxAcademico = nivelMaxAcademico;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSemblanza() {
        return semblanza;
    }

    public void setSemblanza(String semblanza) {
        this.semblanza = semblanza;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public TipoDocBean getTipo() {
        return tipo;
    }

    public void setTipo(TipoDocBean tipo) {
        this.tipo = tipo;
    }

    public PersonaBean getPersona() {
        return persona;
    }

    public void setPersona(PersonaBean persona) {
        this.persona = persona;
    }
    
    

}
