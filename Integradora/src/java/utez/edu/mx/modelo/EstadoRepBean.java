/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class EstadoRepBean {
 private int id;
 private String estadoRep;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstadoRep() {
        return estadoRep;
    }

    public void setEstadoRep(String estadoRep) {
        this.estadoRep = estadoRep;
    }
 
}
