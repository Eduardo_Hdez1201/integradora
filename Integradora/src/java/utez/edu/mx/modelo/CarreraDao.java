/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.servicios.Conexion;

/**
 *
 * @author lfern
 */
public class CarreraDao {

    private Connection c;
    private ResultSet rs;
    private PreparedStatement pst;

    public List<CarreraBean> getCarrerasEspecificas(CarreraBean beanc) {
        List<CarreraBean> listaCarreras = new ArrayList<>();
        CarreraBean carrera = null; 
        try {
            c = Conexion.getConnection();
            pst = c.prepareStatement("select * from carrera where division = ?;");
            pst.setInt(1, beanc.getId());
            rs = pst.executeQuery();
            while (rs.next()) {
                carrera = new CarreraBean();
                carrera.setId(rs.getInt("idCarrera"));
                carrera.setNombre(rs.getString("nombre_carrera"));
                carrera.setNivelAcademico(rs.getString("nivel_academico"));
                carrera.setIdentificador(rs.getString("identificador"));
                listaCarreras.add(carrera);

            }
            pst.close();
            c.close();
            rs.close();

        } catch (SQLException ex) {
            System.err.println("Error getPersonas" + ex);
        }

        return listaCarreras;
    }
    
    private Connection con;
    private PreparedStatement ps;

    private String sqlRegistrarCarrera = "INSERT INTO carrera (id, identificador, nivel_academico, nombre, estado, division) VALUES(NULL,?,?,?,1,?);";

    public boolean registrarCarrera(CarreraBean carrera) {
        boolean resultado = false;
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlRegistrarCarrera);
            ps.setString(1, carrera.getIdentificador());
            ps.setString(2, carrera.getNivelAcademico());
            ps.setString(3, carrera.getNombre());
            //ps.setBoolean(4, carrera.isEstado());
            ps.setInt(4, carrera.getDivision().getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: registrarCarrera()\n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:registrarCarrera()\n" + e);
            }
        }
        return resultado;
    }

    private String sqlModificarCarrera = "UPDATE carrera SET identificador=?, nivel_academico=?, nombre=?, division=? WHERE id = ?;";

    public boolean modificarCarrera(CarreraBean carrera) {
        boolean resultado = false;
        try {
            con = Conexion.getConnection();
            System.out.println("Conexion");
            ps = con.prepareStatement(sqlModificarCarrera);
            System.out.println(carrera.getIdentificador());
            ps.setString(1, carrera.getIdentificador());
            System.out.println(carrera.getNivelAcademico());
            ps.setString(2, carrera.getNivelAcademico());
            System.out.println(carrera.getNombre());
            ps.setString(3, carrera.getNombre());
            System.out.println(carrera.getDivision().getId());
            ps.setInt(4, carrera.getDivision().getId());
            System.out.println(carrera.getId());
            ps.setInt(5, carrera.getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: modificarCarrera()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:modificarCarrera()\n" + e);
            }
        }
        return resultado;
    }

    private String sqlConsultarCarreraEspecifica = "SELECT c.id, c.identificador, c.nivel_academico, c.nombre, d.id as idDivision, d.nombre as division FROM carrera as c INNER JOIN division as d ON c.division = d.id WHERE c.id = ?;";

    public CarreraBean consultarDatosCarrera(CarreraBean carreraConsulta) {
        CarreraBean carrera = null;
        try {
            con = Conexion.getConnection();
            System.out.println("Conexion");
            ps = con.prepareStatement(sqlConsultarCarreraEspecifica);
            System.out.println(" Preparando ...");
            ps.setInt(1, carreraConsulta.getId());
            System.out.println(carreraConsulta.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
                carrera = new CarreraBean();
                carrera.setId(rs.getInt("id"));
                carrera.setIdentificador(rs.getString("identificador"));
                carrera.setNivelAcademico(rs.getString("nivel_academico"));
                carrera.setNombre(rs.getString("nombre"));
                //carrera.setEstado(rs.getBoolean("estado"));

                DivisionBean division = new DivisionBean();
                division.setId(rs.getInt("idDivision"));
                division.setNombre(rs.getString("division"));

                carrera.setDivision(division);
                System.out.println(carrera.getNombre() + " " + carrera.getIdentificador());
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: consultarDatosCarrera()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:consultarDatosCarrera()\n" + e);
            }
        }
        return carrera;
    }

    private String sqlEliminarCarrera = "DELETE FROM carrera WHERE  id = ?;";
    private String sqlEliminarCarreraLogica = "UPDATE carrera SET estado=0 WHERE id = ?;";

    public boolean eliminarCarrera(CarreraBean carrera) {
        boolean resultado = false;
        try {
            System.out.println("entro");
            con = Conexion.getConnection();
            System.out.println("conexion");
            ps = con.prepareStatement(sqlEliminarCarreraLogica);
            System.out.println("-> "+ carrera.getId());
            ps.setInt(1, carrera.getId());
            resultado = ps.executeUpdate() == 1;
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: eliminarCarrera()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:eliminarCarrera()\n" + e);
            }
        }
        return resultado;
    }

    private String sqlConsultarCarreras = "SELECT c.id, c.identificador, c.nivel_academico, c.nombre, d.id as idDivision ,d.nombre as division FROM carrera as c INNER JOIN division as d ON c.division = d.id AND c.estado = 1;";

    public List<CarreraBean> consultarCarreras() {
        List<CarreraBean> carreras = new ArrayList<CarreraBean>();
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlConsultarCarreras);
            rs = ps.executeQuery();
            while (rs.next()) {
                CarreraBean carrera = new CarreraBean();
                carrera.setId(rs.getInt("id"));
                carrera.setIdentificador(rs.getString("identificador"));
                carrera.setNivelAcademico(rs.getString("nivel_academico"));
                carrera.setNombre(rs.getString("nombre"));
                //carrera.setEstado(rs.getBoolean("estado"));

                DivisionBean division = new DivisionBean();
                division.setId(rs.getInt("idDivision"));
                division.setNombre(rs.getString("division"));
                carrera.setDivision(division);

                carreras.add(carrera);
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: consultarCarreras()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:consultarCarreras()\n" + e);
            }
        }
        return carreras;
    }

    private String sqlConsultarDivisiones = "SELECT * FROM division as d INNER JOIN persona as p ON d.director = p.id;";

    public List<DivisionBean> consultarDivisiones() {
        List<DivisionBean> divisiones = new ArrayList<DivisionBean>();
        try {
            con = Conexion.getConnection();
            ps = con.prepareStatement(sqlConsultarDivisiones);
            rs = ps.executeQuery();
            while (rs.next()) {
                DivisionBean division = new DivisionBean();
                division.setId(rs.getInt("id"));
                division.setNombre(rs.getString("nombre"));
                division.setAbreviatura(rs.getString("abreviatura"));

                PersonaBean director = new PersonaBean();
                director.setId(rs.getInt("id"));
                director.setNombre(rs.getString("nombre"));
                director.setPrimerApellido(rs.getString("primer_apellido"));

                division.setDirector(director);

                divisiones.add(division);
            }
        } catch (Exception e) {
            System.err.println("Error\nClase:CarreraDao\nMétodo: consultarDivisiones()\n" + e);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("Error al cerrar conexión.\nClase:CarreraDao\nMétodo:consultarDivisiones()\n" + e);
            }
        }
        return divisiones;
    }
}
