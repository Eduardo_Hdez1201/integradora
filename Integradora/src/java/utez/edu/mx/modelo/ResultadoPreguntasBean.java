/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class ResultadoPreguntasBean {
    private int id;
    private int resultado;
    private String comentario;
    private DocenteBean docente;
    private PreguntaBean pregunta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public DocenteBean getDocente() {
        return docente;
    }

    public void setDocente(DocenteBean docente) {
        this.docente = docente;
    }

    public PreguntaBean getPregunta() {
        return pregunta;
    }

    public void setPregunta(PreguntaBean pregunta) {
        this.pregunta = pregunta;
    }
    
}
