/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.modelo;

/**
 *
 * @author lfern
 */
public class GrupoBean {
    private int id;
    private String nombre;
    private CarreraBean carrera;
    private DocenteBean docente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CarreraBean getCarrera() {
        return carrera;
    }

    public void setCarrera(CarreraBean carrera) {
        this.carrera = carrera;
    }

    public DocenteBean getDocente() {
        return docente;
    }

    public void setDocente(DocenteBean docente) {
        this.docente = docente;
    }
    
}
