/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.ERROR;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import utez.edu.mx.modelo.AspiranteBean;
import utez.edu.mx.modelo.LoginBean;
import utez.edu.mx.modelo.LoginDao;

/**
 *
 * @author lfern
 */
public class LoginControlador extends ActionSupport {

    private final LoginDao dao = new LoginDao();
    private AspiranteBean beanE = new AspiranteBean();
    private String rol;
    private String mensaje;
    private String usuario;
    private String contrasenha;
    private Map session;

    public String login() {
        System.out.println(usuario +  ' ' + contrasenha);
        session = ActionContext.getContext().getSession();
        LoginBean login = dao.login(usuario, contrasenha);
        if (login != null) {
            session.put("persona", login.getPersona());
            session.put("rol", login.getRol());
            session.put("logueado", true);
            session.put("mensaje" , true);
            rol = login.getRol().getRol();
            switch (rol) {
                case "SE":
                    mensaje = "Bienvenido prrro";
                    return "SE";

                case "SA":
                    return "SA";

                case "DD":
                    return "DD";

                case "AS":
                    return "AS";
                
                case "PP":
                    return "PP";    

                default:
                    return ERROR;
            }

        } else {
            return ERROR;
        }
    }

    public String cerrarSesion() {
        session = ActionContext.getContext().getSession();
        session.clear();
        session = null;
        System.out.println(session);
        return SUCCESS;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenha() {
        return contrasenha;
    }

    public void setContrasenha(String contrasenha) {
        this.contrasenha = contrasenha;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Map getSession() {
        return session;
    }

    public void setSession(Map session) {
        this.session = session;
    }

}
