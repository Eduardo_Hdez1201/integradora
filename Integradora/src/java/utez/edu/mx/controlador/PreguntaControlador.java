/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utez.edu.mx.modelo.PreguntaBean;
import utez.edu.mx.modelo.PreguntaDao;

/**
 *
 * @author cds-8B
 */
public class PreguntaControlador {

    private PreguntaBean bean = new PreguntaBean();
    private PreguntaDao dao;
    private List<PreguntaBean> lista;
    private String mensaje = "";
    private Map respuesta;
    private List listaPreguntas = new ArrayList();

    public PreguntaBean getBean() {
        return bean;
    }

    public void setBean(PreguntaBean bean) {
        this.bean = bean;
    }

    public PreguntaDao getDao() {
        return dao;
    }

    public void setDao(PreguntaDao dao) {
        this.dao = dao;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List getListaPreguntas() {
        return listaPreguntas;
    }

    public void setListaPreguntas(List listaPreguntas) {
        this.listaPreguntas = listaPreguntas;
    }

    public String registrarPregunta() {
        dao = new PreguntaDao();
        boolean resultado = dao.registrarPregunta(bean);

        if (resultado) {
            return SUCCESS;
        } else {
            mensaje = "Error";
            return ERROR;

        }
    }

    public String consultarPreguntas() {
        respuesta = new HashMap();
        System.out.println("*************************************************************************");
        lista = dao.consultarPreguntas();
        respuesta.put("listaPreguntas", lista);
        return SUCCESS;
    }

    public String actualizarPregunta() {
        return null;
    }

    public String consultarPreguntaEspecifica() {
        return null;
    }

    public String eliminarPregunta() {

        return null;
    }

}
