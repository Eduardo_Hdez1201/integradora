/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.modelo.AspiranteBean;
import utez.edu.mx.modelo.AspiranteDao;
import utez.edu.mx.modelo.CarreraBean;
import utez.edu.mx.modelo.DocenteBean;
import utez.edu.mx.modelo.DocenteDao;
import utez.edu.mx.modelo.EstadoCivilBean;
import utez.edu.mx.modelo.EstadoRepBean;
import utez.edu.mx.modelo.PreparatoriaBean;

/**
 *
 * @author lfern
 */
public class RegistroControlador {

    private AspiranteBean bean;
    private String mensaje = "";
    private AspiranteDao dao;
    private String usuario;
    private String psw;
    private ArrayList<EstadoRepBean> listaRap = new ArrayList<>();
    private ArrayList<EstadoCivilBean> listaCivil = new ArrayList<>();
    private ArrayList<PreparatoriaBean> listaPrepa = new ArrayList<>();
    private ArrayList<CarreraBean> listaCarrera = new ArrayList<>();

    public String registrarAspirante() {
        dao = new AspiranteDao();
        boolean resultado = dao.registroAspirante(bean, usuario, psw);
        if (resultado) {
            return SUCCESS;
        } else {
           mensaje = "error";
            return ERROR;
        }
    }

    public String consultaRegistro() {
        dao = new AspiranteDao();

        listaCarrera = dao.consultaCarreras();

        listaCivil = dao.consultaEstadoCivil();

        listaPrepa = dao.consultaPreparatorias();

        listaRap = dao.consultaEstadosRepublica();

        return SUCCESS;
    }

    public AspiranteBean getBean() {
        return bean;
    }

    public void setBean(AspiranteBean bean) {
        this.bean = bean;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public ArrayList<EstadoRepBean> getListaRap() {
        return listaRap;
    }

    public void setListaRap(ArrayList<EstadoRepBean> listaRap) {
        this.listaRap = listaRap;
    }

    public List<EstadoCivilBean> getListaCivil() {
        return listaCivil;
    }

    public void setListaCivil(ArrayList<EstadoCivilBean> listaCivil) {
        this.listaCivil = listaCivil;
    }

    public ArrayList<PreparatoriaBean> getListaPrepa() {
        return listaPrepa;
    }

    public void setListaPrepa(ArrayList<PreparatoriaBean> listaPrepa) {
        this.listaPrepa = listaPrepa;
    }

    public ArrayList<CarreraBean> getListaCarrera() {
        return listaCarrera;
    }

    public void setListaCarrera(ArrayList<CarreraBean> listaCarrera) {
        this.listaCarrera = listaCarrera;
    }

}
