/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utez.edu.mx.modelo.DocenteBean;
import utez.edu.mx.modelo.DocenteDao;

/**
 *
 * @author Rebecca Lanuss
 */
public class DocentesAction {
    
       
    private DocenteBean bean = new DocenteBean();
    private DocenteDao dao = new DocenteDao();
    private String mensaje = "";
    private List listaDocentes = new ArrayList();

    public DocenteBean getBean() {
        return bean;
    }

    public void setBean(DocenteBean bean) {
        this.bean = bean;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List getListaDocentes() {
        return listaDocentes;
    }

    public void setListaDocentes(List listaDocentes) {
        this.listaDocentes = listaDocentes;
    }
    
   public String consultarDocentes(){
        listaDocentes = dao.consultarDocentes();
        return SUCCESS;
    }
   
    public String consultarDocentesId() throws SQLException{
        bean = dao.consultarDocentesId(bean.getId());
        if (bean!= null) {
            return SUCCESS;
        } else {
            mensaje = "No se encontro el docente especificado";
            return ERROR;
        }
    }
    
     public String consultarDocentesIdM() throws SQLException{
        bean = dao.consultarDocentesIdM(bean.getId());
        if (bean!= null) {
            return SUCCESS;
        } else {
            mensaje = "No se encontro el docente especificado";
            return ERROR;
        }
    }
     
      public String eliminarDocente(){
        if(dao.EliminarDocente(bean.getId())){
            return SUCCESS;
        }else{
            mensaje ="No se eliminó correctamente";
            return ERROR;
        }
        
        
    } 
      
         public String modificarDocente() {
            if (dao.modificarDocente(bean)) {
            return SUCCESS;
        } else {
            mensaje = "No se modifico correctamente";
            return ERROR;
        }
    }
     
     
}

