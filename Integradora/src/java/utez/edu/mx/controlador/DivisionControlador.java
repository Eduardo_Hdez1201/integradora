/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utez.edu.mx.modelo.CarreraBean;
import utez.edu.mx.modelo.DivisionBean;
import utez.edu.mx.modelo.DivisionDao;
import utez.edu.mx.modelo.PersonaBean;

/**
 *
 * @author cds-8B
 */
public class DivisionControlador {

    private Map respuesta;
    private List<DivisionBean> listaDv;
    private List<CarreraBean> listaC;
    private final DivisionDao dao = new DivisionDao();
    private boolean sesion;
    private DivisionBean beanD;
    private CarreraBean beanc;

    public Map getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Map respuesta) {
        this.respuesta = respuesta;
    }

    public boolean isSesion() {
        return sesion;
    }

    public void setSesion(boolean sesion) {
        this.sesion = sesion;
    }

    public DivisionBean getBeanD() {
        return beanD;
    }

    public void setBeanD(DivisionBean beanD) {
        this.beanD = beanD;
    }

    public CarreraBean getBeanc() {
        return beanc;
    }

    public void setBeanc(CarreraBean beanc) {
        this.beanc = beanc;
    }

    public String consultarDivisiones() {
        respuesta = new HashMap();
        listaDv = dao.getDivisiones();
        listaC = dao.getCarreras();
        respuesta.put("listaDivisiones", listaDv);
        respuesta.put("listaCarreras", listaC);
        return SUCCESS;
    }

    private DivisionDao divisionDao = new DivisionDao();
    private DivisionBean divisionBean = new DivisionBean();
    private DivisionBean divisionBeanModificar = new DivisionBean();
    private List<DivisionBean> divisiones = new ArrayList<DivisionBean>();
    private List<PersonaBean> directores = new ArrayList<PersonaBean>();
    private String respuestas;
    
    public String registrarDivision(){
        if (divisionDao.registrarDivision(divisionBean)) {
            respuestas = "División registrada exitosamente.";
        }else{
            respuestas = "La división no se registró.";
        }
        return SUCCESS;
    }
    
    public String modificarDivision(){
        if (divisionDao.modificarDivision(divisionBeanModificar)) {
            respuestas = "División modificada existosamente.";
        }else{
            respuestas = "No se pudo modificar la división.";
        }
        return SUCCESS;
    }
    
    public String consultarDivisionEspecifica(){
        directores = divisionDao.consultarDirectores();
        divisionBeanModificar = divisionDao.consultarDatosDivision(divisionBeanModificar);
        return SUCCESS;
    }
    
    public String consultarDivision(){
        try {
            directores = divisionDao.consultarDirectores();
            divisiones = divisionDao.consultarDivisiones();
            return "success";
        } catch (Exception e) {
            System.out.println("Error -> "+e);
            return "error";
        }
    }
    
    public String eliminarDivision(){
        System.out.println("Eliminar división");
        if (divisionDao.eliminarDivision(divisionBean)) {
            respuestas = "División eliminada exitosamente.";
            System.out.println(respuesta);
            return "success";
        }else{
            respuestas = "No se pudo eliminar la división.";
            System.out.println(respuesta);
            return "errorAlEliminar";
        }
    }

    public DivisionBean getDivisionBean() {
        return divisionBean;
    }

    public void setDivisionBean(DivisionBean divisionBean) {
        this.divisionBean = divisionBean;
    }

    public DivisionBean getDivisionBeanModificar() {
        return divisionBeanModificar;
    }

    public void setDivisionBeanModificar(DivisionBean divisionBeanModificar) {
        this.divisionBeanModificar = divisionBeanModificar;
    }

    public List<DivisionBean> getDivisiones() {
        return divisiones;
    }

    public void setDivisiones(List<DivisionBean> divisiones) {
        this.divisiones = divisiones;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public List<PersonaBean> getDirectores() {
        return directores;
    }

    public void setDirectores(List<PersonaBean> directores) {
        this.directores = directores;
    }
    
}
