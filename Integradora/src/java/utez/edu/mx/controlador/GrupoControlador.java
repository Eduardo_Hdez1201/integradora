/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utez.edu.mx.modelo.AspiranteBean;
import utez.edu.mx.modelo.CarreraBean;
import utez.edu.mx.modelo.DivisionBean;
import utez.edu.mx.modelo.DocenteBean;
import utez.edu.mx.modelo.GrupoBean;
import utez.edu.mx.modelo.GrupoDao;

/**
 *
 * @author Eduardo
 */
public class GrupoControlador extends ActionSupport {

    private Map respuesta;
    private Map respuestaD;
    private String mensaje ;
    private String carreraSe;
    private String nombre;
    private int id;
    private List<GrupoBean> lista, listaG;
    private List<DocenteBean> listaD;
    private List<DivisionBean> listaDv;
    private List<CarreraBean> listaC;
    private GrupoDao dao = new GrupoDao();
    private boolean sesion;
    private GrupoBean bean;
    private CarreraBean beanC, carreraE;
    private DocenteBean beanD;
    private AspiranteBean beanA;

    
    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    
    public AspiranteBean getBeanA() {
        return beanA;
    }

    public void setBeanA(AspiranteBean beanA) {
        this.beanA = beanA;
    }

    
    public CarreraBean getBeanC() {
        return beanC;
    }

    public void setBeanC(CarreraBean beanC) {
        this.beanC = beanC;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carreraSe;
    }

    public void setCarrera(String carrera) {
        this.carreraSe = carrera;
    }

    public Map getRespuestaD() {
        return respuestaD;
    }

    public DocenteBean getBeanD() {
        return beanD;
    }

    public void setBeanD(DocenteBean beanD) {
        this.beanD = beanD;
    }

    public Map getRespuesta() {
        return respuesta;
    }

    public GrupoBean getBean() {
        return bean;
    }

    public void setBean(GrupoBean bean) {
        this.bean = bean;
    }

    public boolean isSesion() {
        return sesion;
    }

    public void setSesion(boolean sesion) {
        this.sesion = sesion;
    }

    public String registrarGrupo() {
        boolean result = false;
        respuesta = new HashMap();
        dao = new GrupoDao();
        result = dao.registrarGrupo(beanC, bean, beanD);
        respuesta.put("mensaje", mensaje);
        if (result) {
           mensaje = "Hola";
            return "success";
        } else {
            return ERROR;
        }

    }

    public String asignarDocente() {
        boolean result = false;
        respuesta = new HashMap();
        dao = new GrupoDao();
        result = dao.registrarGrupoDocente(bean, beanD);
        if (result) {
            return SUCCESS;
        }
        return ERROR;
    }

    public String asignarAspirante() {
        boolean result = false;
        respuesta = new HashMap();
        dao = new GrupoDao();
        result = dao.registrarGrupoAspirante(bean, beanA);
        if (result) {
            return SUCCESS;
        }
        return ERROR;
    }

    public String consultarGrupoDocente() {
        respuesta = new HashMap();
        lista = dao.getGruposDocente();
        listaG = dao.getGrupos();
        listaDv = dao.getDivisiones();
        listaD = dao.getDocentes();
        listaC = dao.getCarreras();
        respuesta.put("listaDocentes", listaD);
        respuesta.put("listaGrupo", lista);
        respuesta.put("listaDivisiones", listaDv);
        respuesta.put("listaCarreras", listaC);
        respuesta.put("listaGrupos", listaG);
        return SUCCESS;
    }

    public String consultarGrupos() {
        respuesta = new HashMap();
        carreraE = new CarreraBean();
        lista = dao.getGruposCarrera(beanC);
        listaDv = dao.getDivisiones();
        listaD = dao.getDocentes();
        listaC = dao.getCarreras();
        carreraE = dao.getCarreraEspecifica(beanC);
        respuesta.put("listaDocentes", listaD);
        respuesta.put("listaGrupos", lista);
        respuesta.put("listaDivisiones", listaDv);
        respuesta.put("listaCarreras", listaC);
        respuesta.put("carrera", carreraE);
        respuesta.put("mensaje", mensaje);
        return SUCCESS;
    }
    
        public String consultarGrupoAspirante() {
        respuesta = new HashMap();
        carreraE = new CarreraBean();
        lista = dao.getGruposCarrera(beanC);
        listaDv = dao.getDivisiones();
        listaD = dao.getDocentes();
        listaC = dao.getCarreras();
        carreraE = dao.getCarreraEspecifica(beanC);
        respuesta.put("listaDocentes", listaD);
        respuesta.put("listaGrupos", lista);
        respuesta.put("listaDivisiones", listaDv);
        respuesta.put("listaCarreras", listaC);
        respuesta.put("carrera", carreraE);
        return SUCCESS;
    }

    public String grupoDetalles() {

        return SUCCESS;
    }

    public String eliminarGrupo() {
        boolean result = false;
        result = dao.eliminarGrupo(bean);

        return SUCCESS;
    }

    public String eliminarGrupoDocente() {
        boolean result = false;
        result = dao.eliminarGrupoDocente(bean);

        return SUCCESS;
    }

    public String consultaEspecificaGrupo() {
        boolean result = false;
        respuesta = new HashMap();
        bean = new GrupoDao().consultaEspecificaGrupo(bean);
        List<DocenteBean> listaDA = dao.getDocentesGrupo(bean);
        listaD = dao.getDocentes();
        List<AspiranteBean> listaA = dao.getAspirantes();
        List<AspiranteBean> listaAsignados = dao.getAspirantesGrupo(bean, beanA);
        respuesta.put("listaAspirantesGrupos", listaAsignados);
        respuesta.put("listaDocentesAsignados", listaDA);
        respuesta.put("listaDocentes", listaD);
        respuesta.put("listaAspirantes", listaA);
        if (bean != null) {
            return SUCCESS;
        } else {
            return ERROR;
        }

    }
}
