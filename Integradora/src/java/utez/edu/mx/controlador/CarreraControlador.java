/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utez.edu.mx.controlador;

import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utez.edu.mx.modelo.CarreraBean;
import utez.edu.mx.modelo.CarreraDao;
import utez.edu.mx.modelo.DivisionBean;
import utez.edu.mx.modelo.DivisionDao;

/**
 *
 * @author cds-8B
 */
public class CarreraControlador {

    private Map respuesta;
    private List<DivisionBean> listaDv;
    private List<CarreraBean> listaC;
    private final CarreraDao dao = new CarreraDao();
    private boolean sesion;
    private DivisionBean beanD;
    private CarreraBean beanc;

    public Map getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Map respuesta) {
        this.respuesta = respuesta;
    }

    public boolean isSesion() {
        return sesion;
    }

    public void setSesion(boolean sesion) {
        this.sesion = sesion;
    }

    public DivisionBean getBeanD() {
        return beanD;
    }

    public void setBeanD(DivisionBean beanD) {
        this.beanD = beanD;
    }

    public CarreraBean getBeanc() {
        return beanc;
    }

    public void setBeanc(CarreraBean beanc) {
        this.beanc = beanc;
    }

    public String consultarCarrerasEspecificas() {   
        respuesta = new HashMap();
        listaC = dao.getCarrerasEspecificas(beanc);
        respuesta.put("listaCarreras", listaC);
        
        return SUCCESS;

    }

    private CarreraDao carreraDao = new CarreraDao();
    private CarreraBean carreraBean = new CarreraBean();
    private CarreraBean carreraBeanModificar = new CarreraBean();
    private List<CarreraBean> carreras = new ArrayList<CarreraBean>();
    private List<DivisionBean> divisiones = new ArrayList<DivisionBean>();
    private String respuestas;

    public String registrarCarrera() {
        System.out.println("entró!");
        if (carreraDao.registrarCarrera(carreraBean)) {
            System.out.println(carreraBean.getNombre() + " " + carreraBean.getIdentificador());
            return "success";
        } else {
            System.out.println("No se pudo registrar");
            return "error";
        }
    }

    public String modificarCarrera() {
        System.out.println("holii");
        if (carreraDao.modificarCarrera(carreraBeanModificar)) {
            respuestas = "Carrera modificada existosamente.";
        } else {
            respuestas = "No se pudo modificar la carrera.";
        }
        System.out.println(respuesta);
        return "success";
    }

    public String consultarCarreraEspecifica() {
        divisiones = carreraDao.consultarDivisiones();
        System.out.println("-> "+ carreraBeanModificar.getId());
        carreraBeanModificar = carreraDao.consultarDatosCarrera(carreraBeanModificar);
        if (carreraBeanModificar != null) {
            System.out.println("Consulta específica -- lleno");
            return "success";
        } else {
            System.out.println("Consulta específica -- valió");
            return "error";
        }
    }

    public String consultarCarreras() {
        try {
            divisiones = carreraDao.consultarDivisiones();
            carreras = carreraDao.consultarCarreras();
            for (int i = 0; i < carreras.size(); i++) {
                System.out.println(carreras.get(i).getNombre() + " " + carreras.get(i).getIdentificador() + " " + carreras.get(i).getNivelAcademico() + " " + carreras.get(i).getId() + " " + carreras.get(i).getDivision().getNombre() + " " + carreras.get(i).getDivision().getId());
            }
            return "success";
        } catch (Exception e) {
            System.out.println("Error -> " + e);
            return "error";
        }
    }

    public String eliminarCarrera() {
        if (carreraDao.eliminarCarrera(carreraBean)) {
            respuestas = "Carrera eliminada exitosamente.";
            System.out.println(respuesta);
            return "success";
        } else {
            respuestas = "No se pudo eliminar la carrera.";
            System.out.println(respuesta);
            return "errorAlEliminar";
        }
    }

    public CarreraBean getCarreraBean() {
        return carreraBean;
    }

    public void setCarreraBean(CarreraBean carreraBean) {
        this.carreraBean = carreraBean;
    }

    public CarreraBean getCarreraBeanModificar() {
        return carreraBeanModificar;
    }

    public void setCarreraBeanModificar(CarreraBean carreraBeanModificar) {
        this.carreraBeanModificar = carreraBeanModificar;
    }

    public List<CarreraBean> getCarreras() {
        return carreras;
    }

    public void setCarreras(List<CarreraBean> carreras) {
        this.carreras = carreras;
    }

    public String getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(String respuestas) {
        this.respuestas = respuestas;
    }

    public List<DivisionBean> getDivisiones() {
        return divisiones;
    }

    public void setDivisiones(List<DivisionBean> divisiones) {
        this.divisiones = divisiones;
    }
}
