package utez.edu.mx.servicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Clase que establece el medio de comunicaci�n con la base de datos hecha en
 * Oracle
 */
public class Conexion {

    private static String ipAddress;
    private static String dbName;
    private static String user;
    private static String password;
    private static String service;
    private static ResourceBundle propiedadesBD;

    /**
     * M�todo que carga el driver, establece la conexi�n.
     *
     * @ return Connection
     *
     */
    public static Connection getConnection() throws SQLException {
        // Realiza la confirmación del controlador
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (propiedadesBD == null) {
            propiedadesBD = ResourceBundle.getBundle("propiedades");
            ipAddress = propiedadesBD.getString("direccion");
            dbName = propiedadesBD.getString("nombre_bd");
            user = propiedadesBD.getString("usuario");
            password = propiedadesBD.getString("contrasena");
            service = propiedadesBD.getString("puerto");;
        }

        return DriverManager.getConnection("jdbc:mysql://" + ipAddress + ":" + service + "/" + dbName + "?useTimezone=true&serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true", user, password);

    }

    public static void main(String[] args) {
        try {
            Connection con = getConnection();
            System.out.println("Conexion efectuada...");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}