-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: integradora
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aspirante`
--

DROP TABLE IF EXISTS `aspirante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aspirante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(70) NOT NULL,
  `colonia` varchar(70) NOT NULL,
  `promedio` float(7,2) NOT NULL,
  `no_faltas` int(11) DEFAULT '0',
  `estado` int(11) NOT NULL,
  `prepa` int(11) NOT NULL,
  `estado_civil` int(11) NOT NULL,
  `persona` int(11) NOT NULL,
  `grupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_estado` (`estado`),
  KEY `fk_prepa` (`prepa`),
  KEY `fk_estado_civil` (`estado_civil`),
  KEY `fk_persona_aspirante` (`persona`),
  KEY `fk_grupo_idx` (`grupo`),
  CONSTRAINT `fk_estado` FOREIGN KEY (`estado`) REFERENCES `estado_rep` (`id`),
  CONSTRAINT `fk_estado_civil` FOREIGN KEY (`estado_civil`) REFERENCES `estado_civil` (`id`),
  CONSTRAINT `fk_grupo` FOREIGN KEY (`grupo`) REFERENCES `grupo_docente` (`id`),
  CONSTRAINT `fk_persona_aspirante` FOREIGN KEY (`persona`) REFERENCES `persona` (`id`),
  CONSTRAINT `fk_prepa` FOREIGN KEY (`prepa`) REFERENCES `prepa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aspirante`
--

LOCK TABLES `aspirante` WRITE;
/*!40000 ALTER TABLE `aspirante` DISABLE KEYS */;
INSERT INTO `aspirante` VALUES (1,'callex','coloniax',8.00,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `aspirante` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-16  2:08:21
