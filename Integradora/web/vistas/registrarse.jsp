<%-- 
    Document   : registrarse
    Created on : 21/11/2019, 11:18:18 AM
    Author     : lfern
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%String context = request.getContextPath();%>
<%@taglib  prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="<%=context%>/css/loginCSS.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <title class="">Inicio de sesión</title>

    </head>
    <body>
        <div class="container">
            <s:if test="$(#mensaje == 'error')">
                <div class="alert alert-danger" role="alert">
                    Error al registrar
                </div>
            </s:if>

            <div class="row">
                <div class="col-md mt-4"></div>
                <form class="text-center" style="color: #757575;" method="POST" action="registrarAspirante">

                    <div class="col-lg mt-4">
                        <div class="card border-info" style="color: #009575">

                            <h5 class="card-header info-color white-text text-center py-3" style="background-color: #009575">


                                <strong style="color: #000000">Registro de aspirante</strong>
                            </h5>

                            <div class="card-body px-lg-5 pt-2" >

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input type="text"  name="bean.persona.nombre"  required="" id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Nombre(s)</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input required="" name="bean.persona.primerApellido" type="text" id="materialRegisterFormLastName" class="form-control">
                                            <label for="materialRegisterFormLastName">Primer Apellido</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input  required="" type="text" name="bean.persona.segundoApellido"  id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Segundo Apellido</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input style="text-transform: uppercase" onchange="myFunction()" name="bean.persona.curp"  id="curp" type="text"  class="form-control">
                                            <label for="materialRegisterFormLastName">CURP</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input type="date" name="bean.persona.anhoNacimiento" id="materialRegisterFormLastName" class="form-control">
                                            <label for="materialRegisterFormLastName">Año de nacimiento</label>
                                        </div>
                                    </div>
                                    <div class="col">

                                        <div class="form-check">
                                            <input class="form-check-input" name="bean.persona.sexo" type="radio" name="exampleRadios" id="exampleRadios1" value="H" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Hombre
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" name="bean.persona.sexo" type="radio" name="exampleRadios" id="exampleRadios2" value="M">
                                            <label class="form-check-label" for="exampleRadios2">
                                                Mujer
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="md-form">
                                            <select name="bean.estado.id" class="custom-select" for="materialRegisterFormLastName">
                                                <option selected>Selccione una opción</option>
                                                <s:iterator value="listaRap" status="stats" var="listaRep">
                                                    <option value="<s:property value="id"/>"><s:property value="estadoRep" /></option>
                                                </s:iterator>
                                            </select>
                                            <label for="materialRegisterFormLastName">Estado dónde radica</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input name="bean.persona.correoPersonal"  required="" type="email"  id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Correo</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input name="bean.calle"  required="" type="text"  id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Calle</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input type="text" name="bean.colonia" id="materialRegisterFormLastName" class="form-control">
                                            <label for="materialRegisterFormLastName">Colonia</label>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="md-form">

                                            <select name="bean.estadoCivil.id" class="custom-select" for="materialRegisterFormLastName">
                                                <option selected>Selccione una opción</option>
                                                <s:iterator value="listaCivil" status="stats" var="listaCivil">
                                                    <option value="<s:property value="id"/>"><s:property value="estadoCivil" /></option>
                                                </s:iterator>
                                            </select>
                                            <label for="materialRegisterFormLastName">Estado Civil</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input name="bean.persona.telefonoCsas"  required=""  type="number"  id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Teléfono de casa</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- First name -->
                                        <div class="md-form">
                                            <input  name="bean.persona.telefonoCelular" type="number"  id="materialRegisterFormFirstName" class="form-control">
                                            <label for="materialRegisterFormFirstName">Teléfono celular</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="col">

                                        <select  name="bean.prepa.id" class="custom-select" for="materialRegisterFormLastName">
                                            <option selected>Seleccione una opción</option>
                                            <s:iterator value="listaPrepa" status="stats" var="listaPrepa">
                                                <option value="<s:property value="id"/>"><s:property value="nombrePrepa" /></option>
                                            </s:iterator>
                                        </select>
                                        <label for="materialRegisterFormLastName">Preparatoria de procedencia</label>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <input name="bean.promedio" type="text" id="materialRegisterFormLastName" class="form-control">
                                            <label for="materialRegisterFormLastName">Promedio final de preparatoria </label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <!-- Last name -->
                                        <div class="md-form">
                                            <select name="bean.carrera.id" class="custom-select" for="materialRegisterFormLastName">
                                                <s:iterator value="listaCarrera" status="stats" var="listaCarrera">
                                                    <option value="<s:property value="id"/>"><s:property value="nombre" /></option>
                                                </s:iterator>
                                            </select>
                                            <label for="materialRegisterFormLastName">Carrera a ingresar</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <input id="usuario" name="usuario" disabled="" type="text" class="form-control">
                                        <label for="usuario">Usuario</label>
                                    </div>  
                                    <div class="col">
                                        <input type="password" name="psw" id="materialRegisterFormEmail" class="form-control">
                                        <label for="materialRegisterFormEmail">Constraseña</label>
                                    </div>  
                                </div>
                                <button class="btn btn-outline-info btn-rounded  my-4 waves-effect z-depth-0" type="submit">
                                    Registrarse
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>   
            <div class="col-md mt-4"></div>
        </div>
    </body>
    <script>

        function myFunction() {

            var str = document.getElementById("curp").value;

            var res = str.substring(str.length, str.length - 3);
            document.getElementById("usuario").value = res;
        }
    </script>


</html>

