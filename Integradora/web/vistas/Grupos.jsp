<%-- 
    Document   : RegistrarGrupo
    Created on : 6/12/2019, 11:59:44 PM
    Author     : Eduardo
--%>

<% String context = request.getContextPath();%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">

        <title>Gestión de grupos</title>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="sweetalert2.all.min.js"></script>
        <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        <!-- Bootstrap CSS -->
        <link href="<%=context%>/css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<%=context%>/css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="sweetalert2.all.min.js"></script>
        <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        <script src="sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2.min.css">
        <!-- font icon -->
        <link href="<%=context%>/css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<%=context%>/css/font-awesome.min.css" rel="stylesheet" />
        <!-- full calendar css-->
        <link href="<%=context%>/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="<%=context%>/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="<%=context%>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="<%=context%>/css/owl.carousel.css" type="text/css">
        <link href="<%=context%>/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link rel="stylesheet" href="<%=context%>/css/fullcalendar.css">
        <link href="<%=context%>/css/widgets.css" rel="stylesheet">
        <link href="<%=context%>/css/style.css" rel="stylesheet">
        <link href="<%=context%>/css/style-responsive.css" rel="stylesheet" />
        <link href="<%=context%>/css/xcharts.min.css" rel=" stylesheet">
        <link href="<%=context%>/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
        <!-- =======================================================
          Theme Name: NiceAdmin
          Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
          Author: BootstrapMade
          Author URL: https://bootstrapmade.com
        ======================================================= -->
    </head>

    <body>
        <!-- container section start -->
        <section id="container" class="">
            <header class="header white-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
                </div>

                <!--logo start-->
                <div class>
                    <a href="index.html" class="logo"><img style="max-width: 80px;width: 80px;max-height: 50px;height: 50px;padding-bottom:2px;" src="<%=context%>/img/logo_utez.png"></a>
                </div>
                <!--logo end-->

                <div class="nav search-row" id="top_menu">

                </div>

                <div class="top-nav notification-row">
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">

                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="username"><s:property value="#session.persona.nombre + ' ' + #session.persona.primerApellido + ' ' + #session.persona.segundoApellido "/></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="#"><i class="icon_profile"></i> Perfil</a>
                                </li>
                                <li>
                                    <a href="<%=context%>/cerrarSesion"><i class="icon_key_alt"></i>Cerrar sesión</a>
                                </li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                </div>
            </header>
            <!--header end-->

            <!--sidebar start-->
            <aside>
                <div id="sidebar" class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a class="" href="<%=context%>/vistas/ServiciosEscolares.jsp">
                                <i class="icon_house_alt"></i>
                                <span>Inicio</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="<%=context%>/consultarDivisiones">
                                <i class="icon_document_alt"></i>
                                <span>Gestión de grupos</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="<%=context%>/consultarGrupos" class="">
                                <i class="icon_document_alt"></i>
                                <span>Preguntas EV.D</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="<%=context%>/consultarGrupos" class="">
                                <i class="icon_document_alt"></i>
                                <span>Evaluación docente</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;" class="">
                                <i class="icon_desktop"></i>
                                <span>Resultados EV.D</span>
                            </a>
                        </li>
                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->

            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <!--overview start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i>Gestión de grupos</h3>
                            <br>
                            <h4 class="page-header">Divisiones Académicas</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <table class="table table-striped table-advance table-hover">                                    <tr>
                                        <th><p>ID</p></th> 
                                        <th><p>División</p></th>
                                        <th><p>Abreviatura</p></th>
                                        <th><p>Director</p></th>
                                        <th><p>Carreras</p></th>
                                    </tr>
                                    <s:iterator value="respuesta.listaDivisiones" status="stat" >
                                        <tbody>

                                        <td><s:property value="#stat.count"/></td>
                                        <td><s:property value="nombre"/></td>
                                        <td><s:property value="abreviatura"/></td>
                                        <td><s:property value="director.nombre + ' ' +director.primerApellido + ' ' + director.segundoApellido"/></td>
                                        <td>
                                            <div class="btn-group">
                                                <form action="<%=context%>/consultarCarrerasEspecificas" mehtod="POST">  
                                                    <input type="hidden" name="beanc.id" value="<s:property value="id"/>">
                                                    <input type="submit" class="btn btn-primary" value="Ver detalles">
                                                </form>
                                            </div>          
                                        </td>
                                    </s:iterator>
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </section>
            </section>

            <!-- javascripts -->
            <script src="<%=context%>/js/jquery.js"></script>
            <script src="<%=context%>/js/jquery-ui-1.10.4.min.js"></script>
            <script src="<%=context%>/js/jquery-1.8.3.min.js"></script>
            <script type="<%=context%>/text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
            <!-- bootstrap -->
            <script src="<%=context%>/js/bootstrap.min.js"></script>
            <!-- nice scroll -->
            <script src="<%=context%>/js/jquery.scrollTo.min.js"></script>
            <script src="<%=context%>/js/jquery.nicescroll.js" type="text/javascript"></script>
            <!-- charts scripts -->
            <script src="<%=context%>/assets/jquery-knob/js/jquery.knob.js"></script>
            <script src="<%=context%>/js/jquery.sparkline.js" type="text/javascript"></script>
            <script src="<%=context%>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
            <script src="<%=context%>/js/owl.carousel.js"></script>
            <!-- jQuery full calendar -->
            <<script src="<%=context%>/js/fullcalendar.min.js"></script>
            <!-- Full Google Calendar - Calendar -->
            <script src="<%=context%>/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
            <!--script for this page only-->
            <script src="<%=context%>/js/calendar-custom.js"></script>
            <script src="<%=context%>/js/jquery.rateit.min.js"></script>
            <!-- custom select -->
            <script src="<%=context%>/js/jquery.customSelect.min.js"></script>
            <script src="<%=context%>/assets/chart-master/Chart.js"></script>

            <!--custome script for all page-->
            <script src="<%=context%>/js/scripts.js"></script>
            <!-- custom script for this page-->
            <script src="<%=context%>/js/sparkline-chart.js"></script>
            <script src="<%=context%>/js/easy-pie-chart.js"></script>
            <script src="<%=context%>/js/jquery-jvectormap-1.2.2.min.js"></script>
            <script src="<%=context%>/js/jquery-jvectormap-world-mill-en.js"></script>
            <script src="<%=context%>/js/xcharts.min.js"></script>
            <script src="<%=context%>/js/jquery.autosize.min.js"></script>
            <script src="<%=context%>/js/jquery.placeholder.min.js"></script>
            <script src="<%=context%>/js/gdp-data.js"></script>
            <script src="<%=context%>/js/morris.min.js"></script>
            <script src="<%=context%>/js/sparklines.js"></script>
            <script src="<%=context%>/js/charts.js"></script>
            <script src="<%=context%>/js/jquery.slimscroll.min.js"></script>
            <script>
                //knob
                $(function () {
                    $(".knob").knob({
                        'draw': function () {
                            $(this.i).val(this.cv + '%')
                        }
                    })
                });

                //carousel
                $(document).ready(function () {
                    $("#owl-slider").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true

                    });
                });

                //custom select box

                $(function () {
                    $('select.styled').customSelect();
                });

                /* ---------- Map ---------- */
                $(function () {
                    $('#map').vectorMap({
                        map: 'world_mill_en',
                        series: {
                            regions: [{
                                    values: gdpData,
                                    scale: ['#000', '#000'],
                                    normalizeFunction: 'polynomial'
                                }]
                        },
                        backgroundColor: '#eef3f7',
                        onLabelShow: function (e, el, code) {
                            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                        }
                    });
                });
            </script>

    </body>

</html>

