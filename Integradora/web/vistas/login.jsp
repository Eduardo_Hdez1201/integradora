    <%-- 
    Document   : ServiciosEscolares
    Created on : 24/11/2019, 07:39:22 PM
    Author     : Eduardo
--%>

<% String context = request.getContextPath();%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%-- 
    Document   : index
    Created on : 6/12/2019, 08:04:43 PM
    Author     : Eduardo
--%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">

        <title>Servicios Escolares</title>

        <!-- Bootstrap CSS -->
        <link href="<%=context%>/css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<%=context%>/css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="sweetalert2.all.min.js"></script>
        <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        <!-- font icon -->
        <link href="<%=context%>/css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<%=context%>/css/font-awesome.min.css" rel="stylesheet" />
        <!-- full calendar css-->
        <link href="<%=context%>/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="<%=context%>/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="<%=context%>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="<%=context%>/css/owl.carousel.css" type="text/css">
        <link href="<%=context%>/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link rel="stylesheet" href="<%=context%>/css/fullcalendar.css">
        <link href="<%=context%>/css/widgets.css" rel="stylesheet">
        <link href="<%=context%>/css/style.css" rel="stylesheet">
        <link href="<%=context%>/css/style-responsive.css" rel="stylesheet" />
        <link href="<%=context%>/css/xcharts.min.css" rel=" stylesheet">
        <link href="<%=context%>/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
        <!-- =======================================================
          Theme Name: NiceAdmin
          Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
          Author: BootstrapMade
          Author URL: https://bootstrapmade.com
        ======================================================= -->
    </head>

    <body class="login-img3-body">

        <div class="container">
            <form class="login-form" action="<%=context%>/login" method="POST">
                <div class="login-wrap">
                    <h2 style="text-align: center;color: #00AB84">Sistema de Gestión de Cursos de Inducción</h2>
                    <p class="login-img"><img style="width: 220px" src="<%=context%>/img/logo-utez.png"></p>
                    
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon_profile"></i></span>
                        <input name="usuario" type="text" class="form-control" placeholder="Usuario" autofocus>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                        <input name="contrasenha" type="password" class="form-control" placeholder="Contraseña">
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Iniciar Sesión</button>
                    <button class="btn btn-info btn-lg btn-block" type="submit">Registrarse</button>
                </div>
            </form>
            <div class="text-right">
                <div class="credits">
                </div>
            </div>
        </div>


    </body>

</html>

